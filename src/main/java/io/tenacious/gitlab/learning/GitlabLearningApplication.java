package io.tenacious.gitlab.learning;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@Slf4j
public class GitlabLearningApplication {

	public static void main(String[] args) {
		log.info("New Changes");
		SpringApplication.run(GitlabLearningApplication.class, args);
	}

}
